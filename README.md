# Zulip Bot - Overview
This project contains a bot for zulip, it needs to be run standalone, because otherwise it doesn't get the needed messages.

# Deployment
The bot can be started differently
* standalone `npm run start-bot`
* with docker `cd Docker ; docker-compose up`

# Overall Workflow
1. Bot is starting and registeres at the zulip server to receive the messages of the channels it's subscribed for
1. the Bot informes the maintainer, that it was started by private message
1. if a message is entered in any of the streams the bot is subscribed, it...
   1. checks if the stream is on the white list
   1. checks if the message id was already handled
      (sometimes zulip server sends messages multiple times)
   1. asks all responders, if they have something to do for this message
   1. if a subscriber needs to do work, it...
      1. adds a reaction emoji to the original message (this tells the users, that something is comming and is used for debugging in case that a response was expected, but didn't occur)  
      1. waits for the generated message
      1. posts the message


# Features
## Jira
### Overview
### Setup
## Color-Code - WIP!
### Overview
### Setup


# Common problems
```
  if (config.realm.endsWith('/api')) {
                   ^
TypeError: Cannot read property 'endsWith' of undefined
```
=> check NODE_ENV-variable and .env-files

# Notes
https://github.com/zulip/zulip/blob/master/zerver/openapi/zulip.yaml

# Getting it to run
In order to use it, you need the following steps:
1. Create a bot in zulip
1. copy the .env.example and save it as .env.development (if you choose a different name, you have to adjust it in Docker/docker-compose.yml)
1. edit the .env.development

Now comes the interesting part.
The bot at this point doesn't receive any messages, you have to add it to the streams that it should listen to.
But: This is not enough, you have to fill zulip_validStreamIds in the configuration.
This is a list of IDs of the streams. I added this second check to avoid, that the bot by accident answers to non-subscribed chats. Normally this should not happen, but... I misconfigured it and it started to answer posts in the Support-Chat... so I added this security measure.

The ID you can get by looking at the requests of your user in the browser or by calling npm run `starter:get-all-subscriptions` which prints out all subscriptions. 
I know it's a bit of work and I plan to improve this, but this is the situation at the moment.

After that, you can start the bot by docker, or using npm run `start-bot`. It will give you a message when it starts, so then you know that it is running.

// import {zulip} from "zulip-js";
// tslint:disable-next-line:no-var-requires no-require-imports
const zulip = require("zulip-js");
import {BotConfig} from "../utils/bot-config";
import * as fs from "fs";


export class ZulipBotMaintenance {

  public getSubscriptions(): void {
    zulip(BotConfig.config.zulip).then((client) => {
      client.streams.subscriptions.retrieve().then(console.log);
    });
  }

  public uploadFile(): void {
    zulip(BotConfig.config.zulip).then((client) => {
      const data = {
        'file1': fs.createReadStream('c:\\windows-version.txt')
      };
      client.callEndpoint('user_uploads', 'POST', data).then(console.log);
    });
  }

  public deleteAllMessages(): void {
    zulip(BotConfig.config.zulip).then((client) => {
      client.streams.subscriptions.retrieve().then(async (stream) => {
        //not implemented await this.deleteReactionsFromStream(stream, client);
        await this.deleteMessagesFromStream(stream, client);
      });
    });
  }

  private async deleteReactionsFromStream(stream, client): Promise<void> {
    const readParams = {
      stream,
      type: 'stream',
      anchor: 1,
      num_before: 1,
      num_after: 4000,
      narrow: [
        {
          'operator': 'sender',
          'operand': BotConfig.config.zulip.username
        },
        {
          "operator": "topic",
          "operand": "please delete",
          "negated": true
        },
        // {
        //   "operator": "sender",
        //   "operand": BotConfig.config.zulip.username
        // },
        // {
        //   "operator": "streamId",
        //   "operand": BotConfig.config.validStreamIds[0]+""
        // },
      ]
    };
    await client.messages.retrieve(readParams).then(async (response) => {
      console.log(response.messages);
      response.messages
        .filter((message) => message.reactions && message.reactions.length > 0)
        .forEach((entry) => console.log(entry.reactions));
    });
  }

  private async deleteMessagesFromStream(stream, client): Promise<void> {
    const readParams = {
      stream,
      type: 'stream',
      anchor: 1,
      num_before: 1,
      num_after: 4000,
      narrow: [
        {
          'operator': 'sender',
          'operand': BotConfig.config.zulip.username
        },
        {
          "operator": "topic",
          "operand": "please delete",
          "negated": true
        },
      ]
    };

    await client.messages.retrieve(readParams).then(async (response) => {
      for (const message of response.messages) {
        // console.log(message);
        console.log(message.id);
        const request = {
          message_id: message.id
        };
        const rc = await client.messages.deleteById(request)
          .catch((error) => console.log(error));
        if (rc && rc.code === 'BAD_REQUEST') {
          await this.removeContent(client, message);
          await this.removeReaction(client, message);
        } else {
          console.log(rc);
        }
      }
    });
  }

  public unsubscribeAll(): void {
    zulip(BotConfig.config.zulip).then((client) => {
      client.streams.subscriptions.retrieve().then((subs) => {
        // console.log(subs);
        subs.subscriptions.forEach((sub) => {
          console.log(sub);
          const what = [];
          what.push(encodeURI(sub.name));
          console.log(what);

          const removeWhat = {
            subscriptions: JSON.stringify(what),
          };

          console.log(removeWhat);
          client.users.me.subscriptions.remove(removeWhat).then((rc) => {
            console.log(rc);
          });
        });
      });
    });
  }

  /**
   * Delete the content of a message, used in case it cannot be deleted
   */
  private async removeReaction(client, message): Promise<void> {

  }

  /**
   * Delete the content of a message, used in case it cannot be deleted
   */
  private async removeContent(client, message): Promise<void> {
    const params1 = {
      message_id: message.id,
      content: '*deleted*',
    };

    await client.messages.update(params1).then(console.log);

    const params2 = {
      message_id: message.id,
      subject: 'please delete'
    };

    await client.messages.update(params2).then(console.log);
  }
}


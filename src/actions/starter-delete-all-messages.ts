import {ZulipBotMaintenance} from "./zulip-bot-maintenance";

new ZulipBotMaintenance().deleteAllMessages();

import {JiraService} from "./jira-service";
import {BotConfig} from "../utils/bot-config";

beforeEach(() => {
  BotConfig.config.jira.regularExpressionForIssues = '(SEE|SETH)-[0-9]{3,4}';
});

test('parse string for SEE in empty string', () => {
  const jiraService = new JiraService();

  let inputString = "";
  let findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(0);

  inputString = undefined;
  findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(0);
});

test('parse string for SEE in simple string - find something', () => {
  const jiraService = new JiraService();

  let inputString = "SEE-1234";
  let findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(1);
  expect(findings[0]).toBe("SEE-1234");

  inputString = "something SEE-1234";
  findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(1);
  expect(findings[0]).toBe("SEE-1234");

  inputString = "SEE-1234 something";
  findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(1);
  expect(findings[0]).toBe("SEE-1234");

  inputString = "see-1234 something";
  findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(1);
  expect(findings[0]).toBe("SEE-1234");

  inputString = "Ready for review: https://adc.host.domain/jira/browse/SETH-7503";
  findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(1);
  expect(findings[0]).toBe("SETH-7503");
});

test('parse string for SEE in simple string - find nothing', () => {
  const jiraService = new JiraService();

  let inputString = "SE-1234";
  let findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(0);

  inputString = "something SEE-S1234";
  findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(0);

  inputString = "SEE 1234 something";
  findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(0);

  inputString = "ee-1234 something";
  findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(0);
});

test('parse string for SEE in simple string - find multiple', () => {
  const jiraService = new JiraService();

  let inputString = "SEE-1234 SEE-4567";
  let findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(2);
  expect(findings[0]).toBe("SEE-1234");
  expect(findings[1]).toBe("SEE-4567");

  inputString = "something SEE-1234 something else SEE-4567 and finally";
  findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(2);
  expect(findings[0]).toBe("SEE-1234");
  expect(findings[1]).toBe("SEE-4567");
});

test('parse string for SEE and SETH in simple string - find multiple', () => {
  const jiraService = new JiraService();

  let inputString = "SEE-1234 SETH-4567";
  let findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(2);
  expect(findings[0]).toBe("SEE-1234");
  expect(findings[1]).toBe("SETH-4567");

  inputString = "something SEE-1234 something else SETH-4567 and finally";
  findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(2);
  expect(findings[0]).toBe("SEE-1234");
  expect(findings[1]).toBe("SETH-4567");

  inputString = "something seE-1234 something else Seth-4567 and finally";
  findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(2);
  expect(findings[0]).toBe("SEE-1234");
  expect(findings[1]).toBe("SETH-4567");

  inputString = "something seE-123 something else Seth-456-7 and finally";
  findings = jiraService.findIssuesInString(inputString);
  expect(findings).toBeDefined();
  expect(findings.length).toBe(2);
  expect(findings[0]).toBe("SEE-123");
  expect(findings[1]).toBe("SETH-456");
});

test('parse branchInfo - 0 branches', () => {
  const jiraService = new JiraService();

  const inputString = '{summaryBean=com.atlassian.jira.plugin.devstatus.rest.SummaryBean@728c47[summary={pullrequest=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@52e6a87a[overall=com.atlassian.jira.plugin.devstatus.summary.beans.PullRequestOverallBean@4ce3f9a0[stateCount=0,state=OPEN,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], build=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@7cd01291[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BuildOverallBean@68d4b9f8[failedBuildCount=0,successfulBuildCount=0,unknownBuildCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], review=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@f9669e0[overall=com.atlassian.jira.plugin.devstatus.summary.beans.ReviewsOverallBean@5c6baf4b[stateCount=0,state=<null>,dueDate=<null>,overDue=false,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], deployment-environment=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@20d37acd[overall=com.atlassian.jira.plugin.devstatus.summary.beans.DeploymentOverallBean@6d050c56[topEnvironments=[],showProjects=false,successfulCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], repository=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@51e0b17f[overall=com.atlassian.jira.plugin.devstatus.summary.beans.CommitOverallBean@50f05dff[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], branch=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@6bce403e[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BranchOverallBean@2eec96ad[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}]},errors=[],configErrors=[]], devSummaryJson={"cachedValue":{"errors":[],"configErrors":[],"summary":{"pullrequest":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":"OPEN","open":true},"byInstanceType":{}},"build":{"overall":{"count":0,"lastUpdated":null,"failedBuildCount":0,"successfulBuildCount":0,"unknownBuildCount":0},"byInstanceType":{}},"review":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":null,"dueDate":null,"overDue":false,"completed":false},"byInstanceType":{}},"deployment-environment":{"overall":{"count":0,"lastUpdated":null,"topEnvironments":[],"showProjects":false,"successfulCount":0},"byInstanceType":{}},"repository":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}},"branch":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}}}},"isStale":true}}\n';
  const findings = jiraService.extractAndParseDevSummaryJson(inputString);
  expect(findings).toBeDefined();
  expect(findings.cachedValue).toBeDefined();
  expect(findings.cachedValue.summary).toBeDefined();
  expect(findings.cachedValue.summary.branch).toBeDefined();
  expect(findings.cachedValue.summary.branch.overall).toBeDefined();
  expect(findings.cachedValue.summary.branch.overall.count).toBeDefined();
  expect(findings.cachedValue.summary.branch.overall.count).toBe(0);
});

test('parse branchInfo - 2 pull requests', () => {
  const jiraService = new JiraService();

  const inputString = '{summaryBean=com.atlassian.jira.plugin.devstatus.rest.SummaryBean@70d18cac[summary={pullrequest=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@6771d25b[overall=com.atlassian.jira.plugin.devstatus.summary.beans.PullRequestOverallBean@792961b8[stateCount=2,state=MERGED,count=2,lastUpdated=2019-03-07T13:12:07.000+0200,lastUpdatedTimestamp=2019-03-07T13:12:07.000+02:00],byInstanceType={stash=com.atlassian.jira.plugin.devstatus.summary.beans.ObjectByInstanceTypeBean@6fda319f[count=2,name=Bitbucket Server]}], build=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@4c9bd43[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BuildOverallBean@6d43448b[failedBuildCount=0,successfulBuildCount=0,unknownBuildCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], review=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@5f55dc3a[overall=com.atlassian.jira.plugin.devstatus.summary.beans.ReviewsOverallBean@2873f431[stateCount=0,state=<null>,dueDate=<null>,overDue=false,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], deployment-environment=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@619977d9[overall=com.atlassian.jira.plugin.devstatus.summary.beans.DeploymentOverallBean@1609107d[topEnvironments=[],showProjects=false,successfulCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], repository=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@67ccaa8a[overall=com.atlassian.jira.plugin.devstatus.summary.beans.CommitOverallBean@50a9083e[count=16,lastUpdated=2019-03-07T13:12:06.000+0200,lastUpdatedTimestamp=2019-03-07T13:12:06.000+02:00],byInstanceType={stash=com.atlassian.jira.plugin.devstatus.summary.beans.ObjectByInstanceTypeBean@d47247b[count=16,name=Bitbucket Server], fecru=com.atlassian.jira.plugin.devstatus.summary.beans.ObjectByInstanceTypeBean@60701787[count=16,name=FishEye / Crucible]}], branch=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@26ea9162[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BranchOverallBean@3e9a9579[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}]},errors=[],configErrors=[]], devSummaryJson={"cachedValue":{"errors":[],"configErrors":[],"summary":{"pullrequest":{"overall":{"count":2,"lastUpdated":"2019-03-07T13:12:07.000+0200","stateCount":2,"state":"MERGED","open":false},"byInstanceType":{"stash":{"count":2,"name":"Bitbucket Server"}}},"build":{"overall":{"count":0,"lastUpdated":null,"failedBuildCount":0,"successfulBuildCount":0,"unknownBuildCount":0},"byInstanceType":{}},"review":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":null,"dueDate":null,"overDue":false,"completed":false},"byInstanceType":{}},"deployment-environment":{"overall":{"count":0,"lastUpdated":null,"topEnvironments":[],"showProjects":false,"successfulCount":0},"byInstanceType":{}},"repository":{"overall":{"count":16,"lastUpdated":"2019-03-07T13:12:06.000+0200"},"byInstanceType":{"stash":{"count":16,"name":"Bitbucket Server"},"fecru":{"count":16,"name":"FishEye / Crucible"}}},"branch":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}}}},"isStale":false}}\n';
  const findings = jiraService.extractAndParseDevSummaryJson(inputString);
  expect(findings).toBeDefined();
  expect(findings.cachedValue).toBeDefined();
  expect(findings.cachedValue.summary).toBeDefined();
  expect(findings.cachedValue.summary.pullrequest).toBeDefined();
  expect(findings.cachedValue.summary.pullrequest.overall).toBeDefined();
  expect(findings.cachedValue.summary.pullrequest.overall.count).toBeDefined();
  expect(findings.cachedValue.summary.pullrequest.overall.count).toBe(2);
});


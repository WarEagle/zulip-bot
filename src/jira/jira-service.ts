/* tslint:disable:no-reference */
/// <reference path="../../node_modules/jira-client-tools/typings/jira-client.d.ts"/>
/// <reference path="../../node_modules/jira-client-tools/typings/jira-types.d.ts"/>

import * as JiraApi from "jira-client";
import {BotConfig} from "../utils/bot-config";
import {isNullOrUndefined} from "util";
import {JiraTypes} from "jira-client-tools/typings/jira-types";
import {JiraClientTools} from "jira-client-tools/lib/jira-tools";
import IJiraIssue = JiraTypes.IJiraIssue;
import DevSummaryJson = JiraTypes.DevSummaryJson;
import {PrivateMessageSender} from "../private-message-sender";
import JiraRestResponse = JiraTypes.JiraRestResponse;

export class JiraService {
  private readonly privateMessageSender: PrivateMessageSender = new PrivateMessageSender();
  /**
   * Indicator, that the authentication to Jira failed, in this case don't try over and over again, because if will block the user.
   */
  private authFailed: boolean = false;

  private async ensureConnection(): Promise<JiraApi> {
    return new JiraApi({
      protocol: BotConfig.config.jira.serverProtocol,
      host: BotConfig.config.jira.serverAddress,
      port: BotConfig.config.jira.serverPort,
      username: BotConfig.config.jira.username,
      password: BotConfig.config.jira.password,
      apiVersion: BotConfig.config.jira.serverProtocolVersion.toFixed(),
      strictSSL: true
    });
  }

  /**
   * Searchs in Jira for a specific key and returns the issue.
   * Throws an error if no issue was found.
   * @param key
   */
  public async findIssueInJira(key: string): Promise<IJiraIssue> {
    if (this.authFailed) {
      console.warn('Jira request not processed due to auth-problems.');

      return undefined;
    }

    return new Promise((resolve, reject) => {
      this.ensureConnection().then(((jiraConnection) => {
        jiraConnection.findIssue(key).then((issue) => {
          resolve(issue);
        }).catch((error: JiraRestResponse) => {
          // tslint:disable-next-line:no-magic-numbers
          if (error && error.statusCode && +error.statusCode === 401) {
            this.authFailed = true;
            this.privateMessageSender.sendPrivateMessage(BotConfig.config.zulip.maintainerUserName, "Jira rejected username and password, *stopping jira-processing*!")
              .catch(console.error);
          }
          console.log(`Got error during jira communication: ${error.statusCode}`);
          reject();
        });
      })).catch((error) => console.error(`Got error during connection to jira: ${error}`));
    });
  }


  public extractAndParseDevSummaryJson(inputString: string): DevSummaryJson {
    return JiraClientTools.extractAndParseDevSummaryJson(inputString);
  }

  public findIssuesInString(inputString: string): string[] {
    if (!BotConfig.config.jira.regularExpressionForIssues){
      throw new Error('Missing BotConfig.config.jira.regularExpressionForIssues');
    }

    const regex = new RegExp(BotConfig.config.jira.regularExpressionForIssues, 'ig');

    const rc: string[] = [];
    if (isNullOrUndefined(inputString) || (inputString.trim().length === 0)) {
      return rc;
    }

    const found = inputString.match(regex);
    if (isNullOrUndefined(found)) {
      return rc;
    }

    found.forEach((a) => {
      rc.push(a.toUpperCase());
    });


    return rc;
  }
}

import {BotConfig} from "./utils/bot-config";

// tslint:disable-next-line:no-var-requires no-require-imports
const zulip = require("zulip-js");

export class PrivateMessageSender {

  public async sendPrivateMessage(recipient: string, text: string): Promise<void> {
    return zulip(BotConfig.config.zulip).then((client) => {
      const params = {
        to: recipient,
        type: 'private',
        content: text,
      };

      return client.messages.send(params);
    });
  }
}

/* tslint:disable:no-any */
import {ZulipMessage} from "./zulip-message";

export interface ZulipEvent {
  type: string;
  message: ZulipMessage;
  flags: any[];
  id: number;
}

export interface ZulipAttachementsResponse {
  upload_space_used: number;
  result: string;
  msg: string;
  attachments: ZulipAttachement[];
}

export interface ZulipAttachement {
  "name": string;
  "messages": [
    {
      "name": number;
      "id": number;
    }
    ];
  "id": number;
  "size": number;
  "create_time": number;
  "path_id": string;
}

/* tslint:disable:no-any */
export interface ZulipMessage {
  id?: number;
  sender_id?: number;
  content?: string;
  recipient_id?: number;
  timestamp?: number;
  client?: string;
  subject?: string;
  subject_links?: any[];
  is_me_message?: false;
  reactions?: any[];
  submessages?: any[];
  sender_full_name?: string;
  sender_short_name?: string;
  sender_email?: string;
  sender_realm_str?: string;
  display_recipient?: string;
  type?: string;
  stream_id?: number;
  avatar_url?: string;
  content_type?: string;
}

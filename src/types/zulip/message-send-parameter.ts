export interface MessageSendParameter {
  to: string;
  type: string;
  subject: string;
  content: string;
}

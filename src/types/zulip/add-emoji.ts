export interface AddEmoji {
  message_id?: number;
  emoji_name: string;
  reaction_type: string;
  emoji_code: string;

}

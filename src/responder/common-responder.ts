import {ZulipMessage} from "../types/zulip/zulip-message";
import {MessageSendParameter} from "../types/zulip/message-send-parameter";
import {AddEmoji} from "../types/zulip/add-emoji";
import {isNullOrUndefined} from "util";

export abstract class CommonResponder {

  /**
   * If it was decided, that a responder should react on a message, this method is called. It is only called if the message is not blocked otherwise.
   * @param client
   * @param messageToRespondTo
   */
  public async respondToMessage(client, messageToRespondTo: ZulipMessage): Promise<void> {
    // console.log();
    // console.info("Got this message:");
    // console.info(messageToRespondTo.content);
    // console.info(messageToRespondTo.id);

    const isRelevant = await this.checkIfRelevant(messageToRespondTo);
    if (!isRelevant) {
      // console.log(`${this.getResponderName()} not relevant`);
      // console.log(messageToRespondTo.content);

      return;
    }

    // console.log("Problem");
    //console.warn("Responding: ");
    //console.log(messageToRespondTo.content);
    await this.postResponse(messageToRespondTo, client);
    await this.markMessage(messageToRespondTo, client);
  }

  /**
   * The responder decides which reaction-icons should be added to the message that triggered the responder to respond.
   * <i>Hint:</i> return undefined if no icon should be added.
   */
  protected abstract async provideMarkerEmoji(): Promise<AddEmoji | undefined>;

  /**
   * Generates the response for this spcecific message, it's the string of all information that should be given to the stream.
   * This message is allowed to contain markdown, but e.g. html will not work.<br>
   * Assume that this is the text you would enter in the chat window.
   * @param messageToRespondTo
   */
  protected abstract async generateAnswer(messageToRespondTo: ZulipMessage): Promise<string>;

  /**
   * This method checks, if the responder needs to react on a message.
   * If multiple responders are active, not everyone will answer every message.
   * @param messageToRespondTo
   */
  protected abstract async checkIfRelevant(messageToRespondTo: ZulipMessage): Promise<boolean>;

  /**
   * The name if the responder, this is used for logging and in case of messages to the maintainer, it helps to identify a problem.
   */
  protected abstract getResponderName(): string;

  private async postResponse(messageToRespondTo: ZulipMessage, client): Promise<void> {
    const answer = await this.generateAnswer(messageToRespondTo);
    if (isNullOrUndefined(answer)) {
      return;
    }
    // Send a message
    const params: MessageSendParameter = {
      to: messageToRespondTo.display_recipient,
      type: 'stream',
      subject: messageToRespondTo.subject,
      content: answer,
    };

    // console.log("Would answer with:");
    // console.log(params);

    return client.messages.send(params);
  }

  private async markMessage(messageToRespondTo: ZulipMessage, client): Promise<void> {
    const emoji = await this.provideMarkerEmoji();
    if (isNullOrUndefined(emoji)) {
      return;
    }

    emoji.message_id = messageToRespondTo.id;

    return client.reactions.add(emoji);
  }

}

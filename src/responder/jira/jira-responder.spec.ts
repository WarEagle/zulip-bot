/* tslint:disable:no-any */
import {JiraResponder} from "./jira-responder";
import {ZulipMessage} from "../../types/zulip/zulip-message";
import {JiraTypes} from "jira-client-tools/typings/jira-types";
import IJiraIssue = JiraTypes.IJiraIssue;
import {BotConfig} from "../../utils/bot-config";

beforeEach(() => {
  BotConfig.config.jira.regularExpressionForIssues = '(SEE|SETH)-[0-9]{3,4}';
});

test('check emoji exists', () => {
  const service = new JiraResponder();
  expect((service as any).provideMarkerEmoji()).toBeDefined();
});

test('generateAnswer empty parameter', async () => {
  const service = new JiraResponder();
  const response = await (service as any).generateAnswer(undefined);
  expect(response).toBeUndefined();
});

test('generateAnswer empty parameter', async () => {
  const service = new JiraResponder();
  let message: ZulipMessage = {content: ""};
  let response = await (service as any).generateAnswer(message);
  expect(response).toBeUndefined();

  message = {content: "SomeText without issue number."};
  response = await (service as any).generateAnswer(message);
  expect(response).toBeUndefined();
});

// tslint:disable-next-line
function generateMessageForTesting(content: string): any {
  return {
    content: content,
    fields: {
      status: {
        name: "A"
      },
      priority: {
        name: "B"
      },
      reporter: {
        name: "C"
      },
      devSummary: {
        cachedValue: {summary: {pullrequest: {overall: {count: 1}}}}
      }
    }
  };
}

async function generateJiraIssue(): Promise<IJiraIssue> {
  return {
    id: undefined,
    key: "",
    expand: "",
    self: "",
    fields: {
      status: {
        name: "A"
      },
      priority: {
        name: "B"
      },
      reporter: {
        name: "C"
      },
      issuetype: {
        name: "D"
      },
      customfield_52480: "devSummaryJson={\"cachedValue\":{\"errors\":[],\"configErrors\":[],\"summary\":{\"pullrequest\":{\"overall\":{\"count\":0,\"lastUpdated\":null,\"stateCount\":0,\"state\":\"OPEN\",\"open\":true},\"byInstanceType\":{}},\"build\":{\"overall\":{\"count\":0,\"lastUpdated\":null,\"failedBuildCount\":0,\"successfulBuildCount\":0,\"unknownBuildCount\":0},\"byInstanceType\":{}},\"review\":{\"overall\":{\"count\":0,\"lastUpdated\":null,\"stateCount\":0,\"state\":null,\"dueDate\":null,\"overDue\":false,\"completed\":false},\"byInstanceType\":{}},\"deployment-environment\":{\"overall\":{\"count\":0,\"lastUpdated\":null,\"topEnvironments\":[],\"showProjects\":false,\"successfulCount\":0},\"byInstanceType\":{}},\"repository\":{\"overall\":{\"count\":0,\"lastUpdated\":null},\"byInstanceType\":{}},\"branch\":{\"overall\":{\"count\":0,\"lastUpdated\":null},\"byInstanceType\":{}}}},\"isStale\":true}}",
    },
  };
}

test('generateAnswer filled parameter', async () => {
  const service = new JiraResponder();
  ((service as any).jiraService as any).findIssueInJira = generateJiraIssue;

  let message: ZulipMessage = generateMessageForTesting("SEE-1234");
  let response = await (service as any).generateAnswer(message);
  expect(response).toBeDefined();
  expect(response.indexOf('SEE-1234')).not.toBe(-1);

  message = generateMessageForTesting("Some Text then the issue SEE-1234 and then more text.");
  response = await (service as any).generateAnswer(message);
  expect(response).toBeDefined();
  expect(response.indexOf('SEE-1234')).not.toBe(-1);

  message = generateMessageForTesting("Some Text SETH-5678 then the issue SEE-1234 and then more text.");
  response = await (service as any).generateAnswer(message);
  expect(response).toBeDefined();
  expect(response.indexOf('SEE-1234')).not.toBe(-1);

  message = generateMessageForTesting("Some Text SETH-5678 then the issue SEE-1234 and then an url http://somehwhere/SEE-8877/else more text.");
  response = await (service as any).generateAnswer(message);
  expect(response).toBeDefined();
  expect(response.indexOf('SEE-1234')).not.toBe(-1);
  expect(response.indexOf('SETH-5678')).not.toBe(-1);
  expect(response.indexOf('SEE-8877')).not.toBe(-1);
});

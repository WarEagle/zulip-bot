import {CommonResponder} from "../common-responder";
import {AddEmoji} from "../../types/zulip/add-emoji";
import {ZulipMessage} from "../../types/zulip/zulip-message";
import {BotConfig} from "../../utils/bot-config";
import {JiraService} from "../../jira/jira-service";

export class JiraResponder extends CommonResponder {
  private jiraService: JiraService = new JiraService();

  protected async provideMarkerEmoji(): Promise<AddEmoji|undefined> {
    return BotConfig.config.emojiJira;
  }

  protected async generateAnswer(messageToRespondTo: ZulipMessage): Promise<string> {

    if (!messageToRespondTo) {
      return undefined;
    }

    let rc = `:jira:`;

    const issues = this.jiraService.findIssuesInString(messageToRespondTo.content);
    if (issues.length === 0) {
      return undefined;
    }

    for (const issueKey of issues) {
      const issue = await this.jiraService.findIssueInJira(issueKey)
        .catch(console.log);
      // console.log(issue);
      if (!issue) {
        return undefined;
      }

      rc = this.generateShortAnswerContent(issue, rc, issueKey);
    }

    return rc;

    // return `:jira:\n\n`
    //   + `|  |  |  |  |\n`
    //   // + `|--|--|--|--|\n`
    //   + `| ${issueKey} | ${summary} | ${statusName} | ${issueType} |`
    //   ;

    // return `:jira:\n**${issueKey}**: ${summary}\nStatus: ${statusName}\nType: ${issueType}`;
    // ```Content of issue```
  }

  //TODO create long answer method
  private generateShortAnswerContent(issue, rc: string, issueKey): string {
    const summary = issue.fields.summary;
    // const statusName = issue.fields.status.name;
    // const priority = issue.fields.priority.name;
    // const reporter = issue.fields.reporter.displayName;
    // const devSummary = this.jiraService.extractAndParseDevSummaryJson(issue.fields.customfield_52480);
    // const pullRequestCount = devSummary.cachedValue.summary.pullrequest.overall.count;

    rc += `  [${issueKey}](https://adc.luxoft.com/jira/browse/${issueKey})  ${summary} `;
    // rc += ``
    //   + `| [${issueKey}](https://adc.luxoft.com/jira/browse/${issueKey}) | ${summary} |\n`
    //   + `|--|--|\n`
    //   + `| ${issue.fields.issuetype.name} | ${reporter} |\n`
    //   + `| ${priority} | ${statusName} |\n`;
    // if (pullRequestCount > 0) {
    //   rc += `| PRs | ${pullRequestCount} |\n`;
    // }

    rc += `\n`
      + `\n`
    ;

    return rc;
  }

  protected async checkIfRelevant(messageToRespondTo: ZulipMessage): Promise<boolean> {
    let isRelevant = false;
    //TODO extract regex to config
    const issues = this.jiraService.findIssuesInString(messageToRespondTo.content);
    if (issues.length > 0) {
      // console.log("checkIfRelevant");
      // console.log(issues);
      isRelevant = true;
    }

    // console.log(`issues = ${issues}`);
    // console.log(`issues = ${issues.length}`);
    // console.log(`checkIfRelevant = ${isRelevant}`);

    return isRelevant;
  }

  protected getResponderName(): string {
    return "ColorCode";
  }

}

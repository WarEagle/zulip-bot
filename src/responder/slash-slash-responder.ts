// DEBUG, not active at the moment, needs overhaul if reactivated

// import {CommonResponder} from "./common-responder";
// import {ZulipMessage} from "../types/zulip/zulip-message";
// import {AddEmoji} from "../types/zulip/add-emoji";
// import {BotConfig} from "../utils/bot-config";
// import {isNullOrUndefined} from "util";
//
// export class SlashSlashResponder extends CommonResponder {
//
//   public async provideMarkerEmoji(): Promise<AddEmoji|undefined> {
//     return BotConfig.config.emojiSlashSlash;
//   }
//
//   protected async generateAnswer(messageToRespondTo: ZulipMessage): Promise<string> {
//     return `Wait a second. Didn't you want to write this?\n\`\`\`${messageToRespondTo.content}//\`\`\``;
//   }
//
//   protected async checkIfRelevant(messageToRespondTo: ZulipMessage): Promise<boolean> {
//     let isRelevant = false;
//     if (!isNullOrUndefined(messageToRespondTo.content) && messageToRespondTo.content.indexOf('//') === -1) {
//       isRelevant = true;
//     }
//
//     return isRelevant;
//   }
//
//   protected getResponderName(): string {
//     return "ColorCode";
//   }
// }

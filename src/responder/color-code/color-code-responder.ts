import {CommonResponder} from "../common-responder";
import {ZulipMessage} from "../../types/zulip/zulip-message";
import {AddEmoji} from "../../types/zulip/add-emoji";
import {isNullOrUndefined} from "util";
import * as fs from "fs";
import {BotConfig} from "../../utils/bot-config";
// tslint:disable-next-line:no-var-requires no-require-imports
const zulip = require("zulip-js");
// tslint:disable-next-line:no-var-requires no-require-imports
const sharp = require('sharp');

export class ColorCodeResponder extends CommonResponder {
  private readonly REGEX_COLOR_CODE = new RegExp('#[0-9a-fA-F]{2,6}', 'ig');

  public async provideMarkerEmoji(): Promise<AddEmoji | undefined> {
    return BotConfig.config.emojiColorCode;
  }

  protected async generateAnswer(messageToRespondTo: ZulipMessage): Promise<string> {
    const codes = this.findColorCodes(messageToRespondTo.content);

    let response = "";
    for (const code of codes) {
      const uri = await new ColorCodeResponder().uploadFile(code);
      response += `[#${code}](${uri})\n`;
    }

    return response;
  }

  protected async checkIfRelevant(messageToRespondTo: ZulipMessage): Promise<boolean> {
    let isRelevant = false;
    const codes = this.findColorCodes(messageToRespondTo.content);
    if (!isNullOrUndefined(messageToRespondTo.content) && codes.length > 0) {
      isRelevant = true;
    }

    return isRelevant;
  }

  constructor() {
    super();
  }

  public async uploadFile(colorCode: string): Promise<void> {
    const filename: string = await this.generateImageFile(colorCode);

    const sendParams = {
      files: fs.createReadStream(filename),
    };

    const uploadResult = await zulip(BotConfig.config.zulip).then((client) => {
      return client.callEndpoint('user_uploads', 'POST', sendParams);
    });


    return uploadResult.uri;
  }

  public async generateImageFile(colorCode): Promise<string> {
    //TODO delete the file
    const fileName = BotConfig.config.tmpFolder + '/' + colorCode + '.png';
    const image = sharp({
        create: {
          width: 300,
          height: 200,
          channels: 4,
          background: '#' + colorCode
        }
      })
        .png({compressionLevel: 9})
    ;
    await image.toFile(fileName);

    return fileName;
  }

  private findColorCodes(inputString: string): string[] {
    const rc: string[] = [];

    if (isNullOrUndefined(inputString)) {
      return rc;
    }

    const found = inputString.match(this.REGEX_COLOR_CODE);
    if (isNullOrUndefined(found)) {
      return rc;
    }

    found.forEach((a) => {
      rc.push(a.toUpperCase().replace('#', ''));
    });

    return rc;
  }

  protected getResponderName(): string {
    return "ColorCode";
  }
}

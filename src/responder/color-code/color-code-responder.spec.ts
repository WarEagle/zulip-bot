/* tslint:disable:no-any */
import {ColorCodeResponder} from "./color-code-responder";

// test('check emoji exists', () => {
//   const service = new JiraResponder();
//   expect((service as any).provideMarkerEmoji()).toBeDefined();
// });

test('generateAnswer empty parameter', async () => {
  const service = new ColorCodeResponder();
  let response = await (service as any).findColorCodes(undefined);
  expect(response).toBeDefined();
  expect(response.length).toBe(0);

  response = await (service as any).findColorCodes("");
  expect(response).toBeDefined();
  expect(response.length).toBe(0);
});

test('easy code', async () => {
  const service = new ColorCodeResponder();
  let response = await (service as any).findColorCodes("#FF00FF");
  expect(response).toBeDefined();
  expect(response.length).toBeDefined();
  expect(response.length).toBe(1);
  expect(response[0]).toBe("FF00FF");

  //lowercase
  response = await (service as any).findColorCodes("#ff00ff");
  expect(response).toBeDefined();
  expect(response.length).toBeDefined();
  expect(response.length).toBe(1);
  expect(response[0]).toBe("FF00FF");
});

test('color code somewhere', async () => {
  const service = new ColorCodeResponder();
  let response = await (service as any).findColorCodes("Some Text #FF00FF and more text.");
  expect(response).toBeDefined();
  expect(response.length).toBeDefined();
  expect(response.length).toBe(1);
  expect(response[0]).toBe("FF00FF");

  //lowercase
  response = await (service as any).findColorCodes("Some Text #ff00ff and more text.");
  expect(response).toBeDefined();
  expect(response.length).toBeDefined();
  expect(response.length).toBe(1);
  expect(response[0]).toBe("FF00FF");
});


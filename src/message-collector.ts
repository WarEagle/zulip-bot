import {ZulipEvent} from "./types/zulip/zulip-event";
import {ZulipMessage} from "./types/zulip/zulip-message";
import {JiraResponder} from "./responder/jira/jira-responder";
import {BotConfig} from "./utils/bot-config";
import {isNullOrUndefined} from "util";
import {ColorCodeResponder} from "./responder/color-code/color-code-responder";
import {MessageFilter} from "./message-filter";
import {PrivateMessageSender} from "./private-message-sender";
import {CommonResponder} from "./responder/common-responder";

// tslint:disable-next-line:no-var-requires no-require-imports
const zulip = require("zulip-js");

export class MessageCollector {
  private messageFilter: MessageFilter = new MessageFilter();
  private privateMessageSender: PrivateMessageSender = new PrivateMessageSender();

  private responder: CommonResponder[] = [];

  // tslint:disable-next-line:no-any
  public async waitForMessage(client, eventParams): Promise<any> {
    let rc;
    rc = await client.events.retrieve(eventParams)
      .then((response) => rc = response)
      .catch((error) => console.error(error));

    return rc;
  }

  public async collectMessages(client, res): Promise<void> {
    // Retrieve events from a queue
    // Blocking until there is an event (or the request times out)
    const eventParams = {
      queue_id: res.queue_id,
      last_event_id: -1,
      dont_block: false
    };

    while (true) {
      const rc = await this.waitForMessage(client, eventParams);
      if (rc && rc.events) {
        for (const event of rc.events) {
          if (eventParams.last_event_id < rc.events[0].id) {
            // it's a valid message, so increase the id, that we don't receive it again
            eventParams.last_event_id = rc.events[0].id;
          }
          await this.processEvent(client, event);
        }
      }
    }
  }

  /**
   * Register a queue at the zulip server for receiving the messages.
   * @param client
   */
  public async registerQueue(client): Promise<void> {
    // Register queue to receive messages for user
    const queueParams = {
      event_types: ["message"],
      apply_markdown: "false",
      include_subscribers: "true",
      // DON'T EVER use all public streams, the bot will write to everything!
      all_public_streams: "false",
    };
    client.queues.register(queueParams).then(async (res) => {
      // console.log(res);
      //console.log(res.subscriptions);
      await this.collectMessages(client, res);
    });
  }

  /**
   * MAIN METHOD
   */
  public start(): void {
    try {

      zulip(BotConfig.config.zulip).then((client) => {
        this.registerQueue(client)
          .then(() => {
            const message = `I've been started.\n`
              + `List of streams: \`${BotConfig.config.validStreamIds}\`\n`
              + `Looking for issues: \`${BotConfig.config.jira.regularExpressionForIssues}\``;

            this.privateMessageSender.sendPrivateMessage(BotConfig.config.zulip.maintainerUserName, message)
              .catch(console.warn);
            this.registerResponder();

          })
          .catch((error) => console.error(error));
      }).then(res => res)
        .catch(err => console.error("1" + err));
    } catch (e) {
      console.log(2);
    }
  }

  private registerResponder(): void {
    this.responder.push(new JiraResponder());
    this.responder.push(new ColorCodeResponder());
    // this.responder.push(new SlashSlashResponder());
  }

  private async processEvent(client, event: ZulipEvent): Promise<void> {
    if (isNullOrUndefined(event.type) || event.type !== 'message') {
      // only process events
      // console.log(event)
      return;
    }

    if (event.message && event.message.sender_email === BotConfig.config.zulip.username) {
      ///TODO is checking for the email the best way?
      // don't process things we posted :) no loop!
      return;
    }

    await this.processMessage(client, event);
  }

  private async processMessage(client, event: ZulipEvent): Promise<void> {
    return this.messageFilter.filterMessage(event.message).then((message) => {
      return this.replyToMessage(client, message);
    }).catch((error) => {
      console.log(error.toString());
    });
  }

  private async replyToMessage(client, message: ZulipMessage): Promise<void> {
    for (const responder of this.responder) {
      await responder.respondToMessage(client, message);
    }
  }
}

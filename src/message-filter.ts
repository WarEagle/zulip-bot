import {ZulipMessage} from "./types/zulip/zulip-message";
import {BotConfig} from "./utils/bot-config";

export class MessageFilter {
  //TODO this array can grow forever, there needs to be some cleanup
  private readonly alreadyAnswered: number[] = [];


  private async checkIfAlreadyAnswered(messageToRespondTo: ZulipMessage): Promise<boolean> {
    let rc = false;

    if (this.alreadyAnswered.indexOf(messageToRespondTo.id) !== -1) {
      rc = true;
    }

    return rc;
  }

  private async checkIfBlocked(messageToRespondTo: ZulipMessage): Promise<boolean> {
    let rc = true;

    if (BotConfig.config.validStreamIds.indexOf(messageToRespondTo.stream_id) !== -1) {
      console.log(`Allowed stream id: ${messageToRespondTo.stream_id}`);
      rc = false;
    } else {
      console.warn(`Blocked stream id: ${messageToRespondTo.stream_id}`);
    }

    return rc;
  }

  public async filterMessage(messageToRespondTo: ZulipMessage): Promise<ZulipMessage> {
    const alreadyAnswered = await this.checkIfAlreadyAnswered(messageToRespondTo);
    if (alreadyAnswered) {
      throw new Error("already answered");
    } else {
      this.alreadyAnswered.push(messageToRespondTo.id);
    }
    const blocked = await this.checkIfBlocked(messageToRespondTo);
    if (blocked) {
      throw new Error("already answered");
    }

    return messageToRespondTo;
  }

}

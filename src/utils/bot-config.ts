import "./load-config";
import {trim} from "lodash";

interface IBotConfig {
  zulip: {
    username: string;
    apiKey: string;
    realm: string;
    maintainerUserName: string;
  };

  tmpFolder: string;
  validStreamIds: number[];

  emojiSlashSlash: {
    emoji_name: string;
    reaction_type: string;
    emoji_code: string;
  };

  emojiJira: {
    emoji_name: string;
    reaction_type: string;
    emoji_code: string;
  };

  emojiColorCode: {
    emoji_name: string;
    reaction_type: string;
    emoji_code: string;
  };

  jira: {
    username: string;
    password: string;

    regularExpressionForIssues: string;

    serverAddress: string;
    serverPort: string;
    serverProtocol: string;
    serverProtocolVersion: number;
  };

}

export class BotConfig {

  /**
   * The current configuration of the zulip bot, contains configuration parameters, login data and so on.
   */
  public static readonly config: IBotConfig = {
    zulip: {
      username: process.env.zulip_username,
      apiKey: process.env.zulip_apiKey,
      realm: process.env.zulip_realm,
      maintainerUserName: process.env.zulip_maintainer_username
    },
    tmpFolder: process.env.tmp_folder,
    validStreamIds: process.env.zulip_validStreamIds ?
      process.env.zulip_validStreamIds.split(',').map((value) => trim(value)).map((value) => parseInt(value))
      : [],
    emojiSlashSlash: {
      emoji_name: process.env.emojiSlashSlash_emoji_name,
      reaction_type: process.env.emojiSlashSlash_reaction_type,
      emoji_code: process.env.emojiSlashSlash_emoji_code
    },
    emojiJira: {
      emoji_name: process.env.emojiJira_emoji_name,
      reaction_type: process.env.emojiJira_reaction_type,
      emoji_code: process.env.emojiJira_emoji_code
    },
    emojiColorCode: {
      emoji_name: process.env.emojiColorCode_emoji_name,
      reaction_type: process.env.emojiColorCode_reaction_type,
      emoji_code: process.env.emojiColorCode_emoji_code
    },
    jira: {
      username: process.env.jira_username,
      password: process.env.jira_password,

      regularExpressionForIssues: process.env.jira_regex,

      serverAddress: process.env.jira_serverAddress,
      serverPort: process.env.jira_serverPort,
      serverProtocol: process.env.jira_serverProtocol,
      serverProtocolVersion: parseInt(process.env.jira_serverProtocolVersion, undefined),
    }
  };
}

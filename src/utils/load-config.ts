import * as dotenv from "dotenv";
import {trim} from "lodash";

dotenv.config();
let path;

const environment = trim(process.env.NODE_ENV);

switch (environment) {
  case "test":
    path = `${__dirname}/../../.env.test`;
    break;
  case "production":
  case "prod":
    path = `${__dirname}/../../.env.production`;
    break;
  case undefined:
  case "development":
  case "develop":
  case "devel":
  case "dev":
  case "":
    path = `${__dirname}/../../.env.development`;
    break;
  default:
    console.error(`NODE_ENV set to unknown value '${environment}'.`);
    process.exit(1);
    break;
}
dotenv.config({path: path});

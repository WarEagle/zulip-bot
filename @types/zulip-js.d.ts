// tslint:disable
declare module 'zulip-js' {
  export function zulip(initialConfig: ZulipConfiguration): Promise<ZulipClient> ; // replace with real interface/method declarations.

  export interface ZulipConfiguration {
  }

  export interface ZulipStreams {
    retrieve(params): Promise<any>;

    getStreamId(params): Promise<any>;

    subscriptions: {
      retrieve(params?): Promise<any>;
    }
    topics: {
      retrieve(params?): Promise<any>;
    }
  }

  export interface ZulipClient {
    config: any,
    callEndpoint: any,
    accounts: any,
    streams: ZulipStreams,
    messages: any,
    queues: any,
    events: any,
    users: any,
    emojis: any,
    typing: any,
    reactions: any,
    server: any,
  }
}

#!groovy

pipeline {
    agent any

    options() {
        buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '3'))
        disableConcurrentBuilds()
        timeout(time: 30, unit: 'MINUTES')
    }

    environment {
        PRODUCT_NAME = "ZulipBot"
        PATH = "${env.NODEJS_HOME}/bin:${env.PATH}"
        DOCKER_IMAGE_NAME = "build-${PRODUCT_NAME.toLowerCase()}-${BRANCH_NAME.toLowerCase()}"
    }

    stages {

        stage('Build image') {
            steps {
                script {
                      def image = docker.build ("${env.DOCKER_IMAGE_NAME}", "-f Docker/Dockerfile-Buildimage Docker")
                }
            }
        }

        stage('Build in Docker') {
            steps {
                wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm']) {
                    script {
                        docker.image("${env.DOCKER_IMAGE_NAME}").inside() {
                            withEnv([
                                /* Override the npm cache directory to avoid: EACCES: permission denied, mkdir '/.npm' */
                                'npm_config_cache=npm-cache',
                                /* set home to our current directory because other bower
                                * nonsense breaks with HOME=/, e.g.:
                                * EACCES: permission denied, mkdir '/.config'
                                */
                                //'HOME=.',
                                'NPM_CONFIG_PROGRESS=false',
                                'NPM_CONFIG_SPIN=false',
                                'NPM_COLOR=false',
                            ]) {
                                  sh 'node -v'
                                  sh 'npm -v'
                                  //TODO remove this, it was added because somehow sass for node 8.x was active, but searched was sass for node 11.x, no idea where the 8.x comes from
                                  // sh 'npm run rebuild-sass'
                                  sh 'set'
                                  sh 'echo $HOME'
                                  sh 'npm install'
                                  sh 'npm test'
                                  sh 'npm build'
                            }
                        }
                    }
                }
            }
        }
    }

    post {
        always {
            script {
                //Dangling Containers
                sh 'docker ps -q -f status=exited | xargs --no-run-if-empty docker rm'
                //Dangling Images
                sh 'docker images -q -f dangling=true | xargs --no-run-if-empty docker rmi'
                //Dangling Volumes
                sh 'docker volume ls -qf dangling=true | xargs -r docker volume rm'

                //sh "docker rmi ${image.id}"
            }
        }
        changed {
            script {
                final def RECIPIENTS = emailextrecipients([
                        [$class: 'DevelopersRecipientProvider'],
                        [$class: 'CulpritsRecipientProvider']
                ])

                step([$class: 'Mailer', notifyEveryUnstableBuild: true, sendToIndividuals: true, recipients: RECIPIENTS])
            }
        }

        failure {
            script {
                final def RECIPIENTS = emailextrecipients([
                        [$class: 'DevelopersRecipientProvider'],
                        [$class: 'CulpritsRecipientProvider']
                ])

                step([$class: 'Mailer', notifyEveryUnstableBuild: true, sendToIndividuals: true, recipients: RECIPIENTS])
            }
        }
    }
}

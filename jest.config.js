module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  "verbose": true,
  "transform": {
    "^.+\\.jsx?$": "babel-jest", // Adding this line solved the issue
    "^.+\\.tsx?$": "ts-jest"
  },
  roots: [
    "./src/"
  ]
};
